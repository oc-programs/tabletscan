--geolyzer calibration utility
local calibration = {}
local c = calibration
c.geolyzer = nil -- to be filled with calling module
c.max_range = 16
c.density = 1.5
c.variation = 0.25
c.default_sureness = 3
c.max_tries = 100
c.max_scans = 100
c.stat = {}
c.default_vectors = {vertical = {0, 0}, horizontal = {1, 0}} --east and {0,1} is south

function c.in_range(value, target_value, spread)
  local diff = math.abs(value - target_value)
  return diff < spread, math.abs(diff)
end

function c.get_density(scanner, height, vectors)
  local zero_layer = 33
  local scanned_column
  if height ~= 0 then
    scanned_column = c.geolyzer.scan(table.unpack(vectors.vertical))
  else
    scanned_column = c.geolyzer.scan(table.unpack(vectors.horizontal))
  end
  local target_block = scanned_column[zero_layer + height]
  return target_block
end

function c.stable_avg(source_func, compare_func, sureness)
  local stability = 0
  local sum = 0
  local diffs = {}
  local avg
  local avg_is_unstable = true
  local counter = 1
  while avg_is_unstable do
    local data = source_func()
    sum = sum + data
    avg = sum / counter
    if data == 0.0 or counter > c.max_scans then
      avg_is_unstable = false
    else
      local avg_valid, diff = compare_func(avg)
      if avg_valid then
        stability = stability + 1
      else
        stability = 0
      end
      table.insert(diffs, diff)
      if stability > sureness then
        avg_is_unstable = false
      end
      counter = counter + 1
    end
  end
  return avg, counter, diffs
end

function c.moda(set)
  local freq = {}
  for i, v in ipairs(set) do
    if freq[v] then
      freq[v] = freq[v] + 1
    else
      freq[v] = 1
    end
  end
  local most_frequent, result
  for k, v in pairs(freq) do
    most_frequent = math.max((most_frequent or v), v)
    if most_frequent == v then result = k end
  end
  return result
end

function c.round(num)
  local int_part, frac = math.modf(num)
  local sign
  if int_part < 0 then
    sign = -1
  else
    sign = 1
  end
  if math.abs(frac) > 0.55 then
    int_part = int_part + sign
  end
  return int_part
end

c.geolyzer = require("component").geolyzer

print("return {")

for cur_height = 0, c.max_range do
  local function valid_density(value)
    return c.in_range(value, c.density, c.variation)
  end
  local function current_scan()
    return c.get_density(c.geolyzer, cur_height, c.default_vectors)
  end
  local hstat = {}
  local scans_sum = 0
  c.stat[cur_height] = hstat
  
  for try = 1, c.max_tries do
    local avg, scans = c.stable_avg(current_scan, valid_density, c.default_sureness)
    hstat[try] = scans
    scans_sum = scans_sum + scans
  end
  hstat.avg = scans_sum / c.max_tries
  io.write(string.format("{height = %s, scans = {min = %s, max = %s}}", cur_height, c.moda(hstat) - c.default_sureness, c.round(hstat.avg) - c.default_sureness))
  if cur_height ~= c.max_range then print(",") end
end
print("}")